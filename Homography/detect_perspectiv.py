import cv2
import numpy as np
import imutils

file = "perspective_squares/plein.png"

img = cv2.imread(file)
canny = cv2.Canny(img,100,200)

(_,cnts,_) = cv2.findContours(canny,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)

cnt = cnts[0]

epsilon = 0.1*cv2.arcLength(cnt,True)
approx = cv2.approxPolyDP(cnt,epsilon,True)

# Source
src = np.float32([(approx[i][0][0],approx[i][0][1]) for i in range(len(approx))])
print("Coins source: {}".format(src))

# Destination
(x, y, w, h) = cv2.boundingRect(cnt)
dst = np.float32([(x,y),(x+w,y),(x+w,y+h),(x,y+h)])
print("Coins destination: {}".format(dst))

# Affichage
cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 2)
# for pt in dst:
#     cv2.circle(img, pt, 10, (255,0,0), -1)
cv2.drawContours(img,approx,-1,(0,0,255),10)


M = cv2.getPerspectiveTransform(dst,src)
new_img = cv2.warpPerspective(img.copy(), M, (2*img.shape[0],2*img.shape[1]))

cv2.imshow('detect',img)
cv2.imshow('correction',new_img)
# cv2.imshow('thresh',thresh)

while(True):
    if cv2.waitKey(1) == 27:
        cv2.destroyAllWindows()

print("FIN")