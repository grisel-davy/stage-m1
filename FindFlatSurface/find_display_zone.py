"""
###############################################################
Code faisant parti du stage de M1 à l'ISRI - Sorbonne Université

Auteur: Arthur Grisel-Davy

But: Détection d'un rectangle représentant le cadre de projection 
possible du video projecteur en se basant sur la différence des
images avant et aprés projection.
###############################################################
"""

import numpy as np
import time
import sys

import cv2



# Get the images
error = True
maxiter=10
iteration=1 # on commence pas à zero pour pas trouver la webcame de l'ordi.
while (iteration<maxiter and error):
    try:
        cam = cv2.VideoCapture(iteration)
        ret_val,img = cam.read()
        img = cv2.resize(img,(100,100))
        error=False
        print("Cam {} fonctionne!".format(iteration))
    except:
        iteration += 1
        print("Cam {} erreur...".format(iteration))
        cam.release()

if(iteration >= maxiter):
    sys.exit("Pas de camera utilisable. Sortie du programme")


print("Première image!")
ret_val, image_avant = cam.read()

print(type(image_avant[0]))
for i in range(50):
    time.sleep(0.1)
    sys.stdout.write("\r%d%%" % i)
    sys.stdout.flush()
    start_flood = time.time()

while(time.time()-start_flood<0.1):
    ret_val,mur = cam.read()

print("Seconde image!")
ret_val, image_apres = cam.read()


# Blur them
blurred_avant = cv2.GaussianBlur(image_avant, (7, 7), 0)
blurred_apres = cv2.GaussianBlur(image_apres, (7, 7), 0)

# Greyscale them
gray_avant = cv2.cvtColor(image_avant, cv2.COLOR_BGR2GRAY)
gray_apres = cv2.cvtColor(image_apres, cv2.COLOR_BGR2GRAY)

del blurred_avant
del blurred_apres

# Compute the structurale similarity index between the two images
(score, diff) = compare_ssim(gray_avant, gray_apres, full=True)
diff = (diff*255).astype("uint8")

# Thresh the result and detect contours
thresh = cv2.threshold(diff, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU )[1]
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)

max_area = 0


# Hightlight the contours
for c in cnts:
    # compute the bounding box of the contour and then draw the
    # bounding box on both input images to represent where the two
    # images differ
    (x, y, w, h) = cv2.boundingRect(c)
    if w*h > max_area:
        max_area = w*h
        tuple_max_c = (x, y, w, h)

#cv2.rectangle(image_apres, (tuple_max_c[0], tuple_max_c[1]), (tuple_max_c[0] + tuple_max_c[2], tuple_max_c[1] + tuple_max_c[3]), (0, 0, 255), 2)

cv2.imshow('Image avant', image_avant)
cv2.imshow('Image apres', image_apres)
cv2.imshow('Diff des images', thresh)

stop=False
while(not stop):
    if(cv2.waitKey(1) == 27):
        cam.release()
        cv2.destroyAllWindows()
        stop=True