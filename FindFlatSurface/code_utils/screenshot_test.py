import cv2
import numpy as np
import pyscreenshot as pss
import screeninfo
import matplotlib.pyplot as plt

cs = screeninfo.get_monitors()[0]# Normalment 0 c'est le principal
ds = screeninfo.get_monitors()[1]# Normalement 1 c'est le secondaire

img = pss.grab()
img = np.array(img)
img = img[cs.y:cs.y+cs.height,cs.x:cs.x+cs.width]

plt.figure("screenshot")
plt.imshow(img)
plt.show()