import numpy as np
import cv2
import screeninfo
from time import time,sleep
import sys, os

# Gestion des écrans pour l'affichage >>>>>>>>>>>>>>>>
cs = screeninfo.get_monitors()[0]# Normalment 0 c'est le principal
ds = screeninfo.get_monitors()[1]# Normalement 1 c'est le secondaire


error = True
maxiter=10
iteration=1 # on commence pas à zero pour pas trouver la webcame de l'ordi.
while (iteration<maxiter and error):
    try:
        cam = cv2.VideoCapture(iteration)
        ret_val,img = cam.read()
        img = cv2.resize(img,(100,100))
        error=False
        print("Cam {} fonctionne!".format(iteration))
    except:
        iteration += 1
        print("Cam {} erreur...".format(iteration))
        cam.release()

if(iteration >= maxiter):
    sys.exit("Pas de camera utilisable. Sortie du programme")

print("Écran de projection: width={},heigh={}".format(ds.width,ds.height))

window = "Projection"
cv2.namedWindow(window, cv2.WND_PROP_FULLSCREEN)
cv2.moveWindow(window, ds.x - 1, ds.y - 1)
cv2.setWindowProperty(window, cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)

# <<<<<<<<<<<<<<<<<<<<<<<<

# Image à Afficher >>>>>>>>>>>>

cadre = np.zeros((ds.height,ds.width,3))# Création du fond à la taille de l'affichage
cadre = cadre.astype('uint8')
cv2.rectangle(cadre,(5,4),(ds.width-5,ds.height-5),(0,0,255),cv2.FILLED)

cv2.imshow(window,cadre)


lower_red = np.array([141, 145, 50])
highter_red = np.array([191, 255, 255])

while(True):
    ret_val,frame = cam.read()
    frame = cv2.blur(frame,(5,5))
    edges = cv2.Canny(frame,70,100) # detect objects


    hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv_frame,lower_red,highter_red)

    _, contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)


    for contour in contours:
        approx = cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True)

        if ((len(approx) == 4)):
            (x, y, w, h) = cv2.boundingRect(approx)
            cv2.drawContours(frame, [contour], 0, (0,255,0), 3)

        # rect = cv2.minAreaRect(contour)
        # box = cv2.boxPoints(rect) # cv2.boxPoints(rect) for OpenCV 3.x
        # box = np.int0(box)
        # cv2.drawContours(frame,[box],0,(0,0,255),2)

    red = cv2.bitwise_and(frame,frame, mask = mask)


    # tresh = cv2.adaptiveThreshold(canal_red,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)

    cv2.imshow("frame",frame)
    cv2.imshow("masked",red)
    cv2.imshow("edges",edges)




    if(cv2.waitKey(1) == 27):
        cv2.imwrite( "cadre_red.png", frame)
        cam.release()
        cv2.destroyAllWindows()