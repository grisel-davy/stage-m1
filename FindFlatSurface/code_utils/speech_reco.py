from pocketsphinx import LiveSpeech
import time

speech = LiveSpeech(lm=False, keyphrase='refresh', kws_threshold = 1e-20)

depart = time.time()
previous = depart

while time.time()-depart<20:

    for phrase in speech:
        print("Detected: {}".format(phrase.segments(detailed=True)))

    if time.time()-previous >1:
        print(time.time())
        previous = time.time()

