import cv2
import numpy as np

mur = cv2.imread('wall_perspect.jpeg',1)
affiche = cv2.imread('affiche3.jpg',1)


pts_dst = np.float32([[309, 191],[309, 285],[542, 269],[539, 154]])
h,w,p = affiche.shape
pts_src = np.float32([[0,0],[0,h],[w,h],[w,0]])

## selection de la zone à transferer
minx = int(min(pts_dst[:,0]))
maxx = int(max(pts_dst[:,0]))
miny = int(min(pts_dst[:,1]))
maxy = int(max(pts_dst[:,1]))

pts_dst_reduit = pts_dst
pts_dst_reduit[:,0] = pts_dst_reduit[:,0]-minx
pts_dst_reduit[:,1] = pts_dst_reduit[:,1]-miny

M = cv2.getPerspectiveTransform(pts_src,pts_dst_reduit)

new_affiche = cv2.warpPerspective(affiche, M,(maxx-minx,maxy-miny))


## collage:
mur[miny:maxy,minx:maxx] = new_affiche[0:maxy-miny,0:maxx-minx]


cv2.imshow("Source Image", affiche)
cv2.imshow("Destination image", mur)
# cv2.imshow("Warped Source Image", new_affiche)


cv2.waitKey(0)
cv2.destroyAllWindows()


print("DONE")