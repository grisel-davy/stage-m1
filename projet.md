# Direction

## Description:

Un vidéo projecteur est monté sur un système mobile dans une pièce. Le but est de faire de la réalité augmentée par projection. Plusieurs cas d'utilisation sont possibles:

* Suivi d'une personne pour afficher des informations proches d'elle.
* Projection sur Une table:
    + Indications pour un travail précis.
    + Mise en évidences d'objects.
    + Travaux du [GIS:](https://lightguidesys.com/blog/projector-based-augmented-reality-new-form-enterprise-ar/) ou de [Microsoft](https://www.microsoft.com/en-us/research/wp-content/uploads/2010/10/Wilson-UIST-2010-LightSpace.pdf)

Ces technologies peuvent trouver des applications dans plusieurs contextes comme les usines (instructions pour le travail à la chaine comme fait le GIS), les atteliers avec des travaux manuels (type horlogerie, travaux ou la personnes à des contraintes de mobilité (les mains prises) et doit avoir des infos pertinentes), les jeux.

Pour cela plusieurs point sont à maitriser:

* Détection des meilleures surfaces de projection (déjà travaillé en grande partie par David POULAIN).
* Homographie pour afficher une image cohérente.
* Détection des intentions d'une personne (déplacements, mains, regard) pour afficher les informations les plus adaptées possibles.


##Objectifs

L'objectif est de réaliser un système permétant de:

* Utiliser plusieurs types d'affichages différents. Deux affichages classiques sont par exemple un affichage sur un mur face à un plan de travail et l'affichage sur une table par dessus au milieux du plan de travail. Ce changement nécéssite de pouvoir déplacer le projecteur (au moins suivant un axe traversant la piece) et le pivoter entre 0° (horizontale classique) et -90° (projection par le haut).
* Réagire à des instructions de l'utilisateur. Pour simplement changer de type d'affichage ou pour des interractions plus complexes (déplacer des affichage, donner des instructions). Il faut choisir un système de communication. 
    + Par un bouton logiciel (seulement dans le cas d'un affichage de bureau). Peut être lié à une combinaison de touches sur un clavier.
    + Par geste. Peut être pratique pour désigner un emplacement mais nécéssite que les mains de l'utilisateur soient libres.
    + Par bouton. Très classique et moins naturelle pour l'utilisateur.
    + Par la voix. Sort un peu du cadre du projet mais permet de gérer le problème des mains prises

Si tout est parfait et que je n'ai plus rien à faire:

* Gérer les interactions avec l'utilisateur. L'utilisateur pourait bouger les zones d'affichage, les vérouiller.
* Gérer les changement de mur. Dans une piece avec des bureau contre deux murs, il est alors possible de suivre l'utilisateur.
* Gérer le changement automatique de type d'affichage. On peut envisager que si l'utilisateur se lève, il obstrue l'affichage sur le mur, on déplace alors le vidéo projecteur sur le coté et on réalise une correction de perspective pour garder un affichage maximal et sans déformations.


##Cas d'utilisation envisagé:
Une personne travail à son bureau. Il peut projeter des informations pertinentes devant lui sur le mur (la où serait un écran d'ordinateur par exemple mais en plus grand. On peut envisager de projecter directement un bureau d'ordinateur et de laisser un clavier et une sourie). Si la personne souhaite travailler sur quelque chose sur sa table (type travail d'horloger), l'affichage passe au dessus de lui (signal de changement à trouver) et choisit le meilleur emplacement sur la table pour retranscrir ce qui était sur le mur avant. Dans les deux cas (deux types d'affichages), il faut choisir le meilleur emplacement car le mur ou la table peuvent être encombrés. On peut envisager de détecter plusieurs espaces d'affichage et d'y projeter plusieurs informations.

##Échéancié
Temps total: 81 jours, 11 semaines.


### Partie 1:
2 semaines.


Prise en main des systèmes existants, vérification du materiel disponible.

### Partie 2:
2 semaines.


Déplacement de la partie mobile. Vérifier si un déplacement est déjà possible. Si non, il faut s'en occuper directement et faire en sorte qu'au moins un déplacement et une rotation soient possibles.

### Partie 3:
3 semaines.


Vérifier la détection des zones affichables. Cette partie à normalement déjà été traitée par David POULAIN et donc devrait être rapidement réutilisable. Elle sera utile pour choisir l'emplacement d'affichage dans un environement encombré. Bien prendre en compte le cas d'oscillation entre deux affichages possibles pour avoir un affichage stable (partie "3.5.4 Limitations" du rapport de David).

### Partie 4:
2 semaines.


Gestion de l'interraction avec l'utilisateur pour passer d'un affichage à l'autre.

### Partie 5:
?? semaines.


Mise en place de l'homographie pour avoir un affichage fidèl même dans des cas de non alignement du projecteur.
Méthode possible: Aprés avoir détecté un zone possible d'affichage (qui n'est plus forcement carré maintenant), on projete une forme connue type quadrillage. On visualise la déformation subit par la perspective et on en déduit l'homographie correspondante. il sufit alors d'appliquer le traitement inverse pour annuler cette perspective.
Avantage: Plus robuste. le robot ne calcule pas la déformation en fonction de sa position dans l'espace. Pas besoin de capteurs de position.
Inconvénient: Il faut une phase de projection aprés chaque mouvement pour recalculer la transformation. Prend du temps et casse l'immersion de l'utilisateur.
[Aide Homographie OpenCV](https://www.learnopencv.com/homography-examples-using-opencv-python-c/)
