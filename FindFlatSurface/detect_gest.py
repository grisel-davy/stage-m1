import cv2
from time import sleep

from utils import *

cam = find_webcam()
font = cv2.FONT_HERSHEY_SIMPLEX

while True:
    _,img = cam.read()
    try:
        img,gest = gesture(img)
        if gest:
            #print("Open")
            cv2.putText(img,'Open',(0,50), font, 2, (0,255,0), 3, cv2.LINE_AA)
        else:
            #print("Close")
            cv2.putText(img,'Close',(0,50), font, 2, (0,0,255), 3, cv2.LINE_AA)
    except:
        print("pass")
    cv2.imshow('cam',img)
    if cv2.waitKey(1)==27:
        cam.release()
        cv2.destroyAllWindows()
        break
