"""
###############################################################
Code faisant parti du stage de M1 à l'ISRI - Sorbonne Université

Auteur de la fonction Max_Rect (et fonctions liées): Idée de JFS (https://stackoverflow.com/users/4279/jfs) et 
réalisation de MiB_Coder (https://stackoverflow.com/users/6792694/mib-coder)
Merci à eux.
Auteur du reste: Arthur Grisel-Davy.

But: Collection de fonctions utiles .
La fonction Max_rect permet de déterminer la zone d'affichage. Elle prend comme
argument une matrice dont les points à 1 sont les obstacles. Cette 
fonction cherche alors le plusgrand rectangle ne contenant aucun 1
et renvoit les informations pertinentes pour l'utiliser.
###############################################################
"""

import cv2
import sys
import imutils
import numpy as np
from time import time
from collections import namedtuple
from skimage.measure import compare_ssim
from matplotlib import pyplot as plt

Info = namedtuple('Info', 'start height')
font = cv2.FONT_HERSHEY_SIMPLEX

def display_color(ds,color,window):
    full = np.zeros((ds.height,ds.width,3))
    full = full.astype('uint8')
    full[0:int(ds.height/10),:] = color # Couleur
    full[int(9*ds.height/10):,:] = color # Couleur
    full[:,0:int(ds.width/10)] = color # Couleur
    full[:,int(9*ds.width/10):] = color # Couleur

    cv2.imshow(window,full)

def find_webcam():
    """ Fonction permétant de créer l'objet camera.
    La camera à utiliser n'est pas forcement accessible. On effectue
    plusieurs test pour déterminer laquelle est la bonne.
    Renvoit l'objet camera qui fonctionne"""

    error = True
    maxiter=10
    iteration=1 # on commence pas à zero pour pas trouver la webcam de l'ordi.
    while (iteration<maxiter and error):
        try:
            cam = cv2.VideoCapture(iteration)
            ret_val,img = cam.read()
            img = cv2.resize(img,(100,100))
            error=False
            print("Cam {} fonctionne!".format(iteration))
        except:
            iteration += 1
            print("Cam {} erreur...".format(iteration))
            cam.release()

    if(iteration >= maxiter):
        sys.exit("Pas de camera trouvé. Sortie du programme")

    for i in range(30):
        iter_val, dummy = cam.read() # Flush the frames used for calibration

    return(cam)

def find_displayable_zone(cam,window,ds):

    display_color(ds,(0,0,0),window)
    depart = time()
    while(time()-depart<1):
        # cv2.imshow(window,full)
        if(cv2.waitKey(1) == 27):
            # Destruction des instance crées
            cv2.destroyAllWindows()
        ret_val,mur = cam.read()

    print("Première image!")
    ret_val, image_avant = cam.read()

    depart = time()
    while(time()-depart<1):
        # cv2.imshow(window,full)
        if(cv2.waitKey(1) == 27):
            # Destruction des instance crées
            cv2.destroyAllWindows()
        ret_val,mur = cam.read()

    display_color(ds,(255,255,255),window)

    depart = time()
    while(time()-depart<1):
        # cv2.imshow(window,full)
        if(cv2.waitKey(1) == 27):
            # Destruction des instance crées
            cv2.destroyAllWindows()
        ret_val,mur = cam.read()


    print("Seconde image!")
    ret_val, image_apres = cam.read()
    '''    
    stop = False
    while(not stop):
        cv2.imshow('avany',image_avant)
        cv2.imshow('apres',image_apres)
        if(cv2.waitKey(1)):
            stop = True
    cv2.waitKey(0)
    '''

    # Blur them
    blurred_avant = cv2.GaussianBlur(image_avant, (7, 7), 0)
    blurred_apres = cv2.GaussianBlur(image_apres, (7, 7), 0)

    # Greyscale them
    gray_avant = cv2.cvtColor(image_avant, cv2.COLOR_BGR2GRAY)
    gray_apres = cv2.cvtColor(image_apres, cv2.COLOR_BGR2GRAY)

    # Compute the structurale similarity index between the two images
    print('comparaison...')
    (score, diff) = compare_ssim(gray_avant, gray_apres, full=True)
    print('done')
    diff = (diff*255).astype("uint8")

    # Thresh the result and detect contours
    print('contours...')
    thresh = cv2.threshold(diff, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU )[1]
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
   
    area_max = 0
    # Hightlight the contours
    # Store the coordinates for the contours with the largest area
    for c in cnts:
        (x, y, w, h) = cv2.boundingRect(c)
        if w*h > area_max:
            area_max = w*h
            tuple_max_c = (x, y, w, h)

    cv2.rectangle(image_apres, (tuple_max_c[0], tuple_max_c[1]), (tuple_max_c[0] + tuple_max_c[2], tuple_max_c[1] + tuple_max_c[3]), (0, 0, 255), 2)
    return(tuple_max_c)

def find_zone_affichage(mur,ratio,ds):
    """Fonction permétant de déterminer la meilleur zone pour 
    l'affichage dans la zone affichable. Se base sur les contours 
    des objects et détermine le plus grand rectangle sans object.
    Renvoit les point définissant le rectangle d'affichage ainsi
    que l'aire et l'image avec la zone dessinée.
    """
    # mur = mur[int(res_cam[0]*bornes[0]):int(res_cam[0]*bornes[1]),int(res_cam[1]*bornes[2]):int(res_cam[1]*bornes[3])]

    mur = cv2.blur(mur,(5,5))
    # blur = cv2.GaussianBlur(img,(5,5),0)

    edges = cv2.Canny(mur,60,100)

    h,w,lc,br=max_rect(edges) # Find max rectangle

    cv2.rectangle(edges,(lc,br-h),(lc+w,br),(255,0,0),2)

    # Application du padding sur la zone d'affichage
    lc = lc+int(w*0.05)
    br = br-int(h*0.05)
    w = int(w*0.9)
    h = int(h*0.9)

    cv2.rectangle(edges,(lc,br-h),(lc+w,br),(0,0,255),2)

    x,y = edges.shape # get geometry
    # print("Image mur: x={},y={}".format(x,y))
    ''' 
    stop = False
    while(not stop):
        cv2.imshow('edges',edges)
        if(cv2.waitKey(1)):
            stop = True
    cv2.waitKey(0)
   '''
   # cv2.imshow("Zone choisie",edges)
    # print("Plus grand rectangle trouvé en ({}:{}) de taille (width={}:height={}) en {} secondes)".format(lc,br-h,w,h,round(fin,3)))

    # Le rectangle rendu est le plus grand rectangle possible sans obstable.
    # Mais ce rectangle ne respecte peut être pas les dimensions de l'image à afficher.
    # On va donc tronquer ce rectangle pour avoir un ratio correcte.

    if(ratio*h>w):
        # cas rectangle trop haut
        new_h = w/ratio
        br = int(br-(h-new_h)/2) #on remonte la bottom row de seulement la moitié pour garder le centre
        h=int(new_h)
    else:
        # cas rectangle trop large
        new_w = h*ratio
        lc = int(lc+(w-new_w)/2)
        w = int(new_w)


    cv2.rectangle(edges,(lc,br-h),(lc+w,br),(100,255,0),2)
    cv2.imshow("Zone choisie parmis les obstacles",edges)

    # Recalcule des points
    pt1 = (int((br-h)/x*ds.height), int(lc/y*ds.width))
    pt2 = (int(br/x*ds.height), int((lc+w)/y*ds.width))

    return(pt1,pt2,w*h)

def max_rect(mat, value=0):
    """returns (height, width, left_column, bottom_row) of the largest rectangle 
    containing all `value`'s.
    """
    it = iter(mat) # Chaque element de it contient une ligne de mat
    hist = [(el==value) for el in next(it, [])] # Pose True ou False sur la ligne suivante de l'iterateur
    max_rect = max_rectangle_size(hist) + (0,)
    for irow,row in enumerate(it): # irow prend l'indice de la ligne (0,1,2,...), row prend la ligne
        hist = [(1+h) if el == value else 0 for h, el in zip(hist, row)]
        max_rect = max(max_rect, max_rectangle_size(hist) + (irow+1,), key=area)
        # irow+1, because we already used one row for initializing max_rect
    return max_rect

def max_rectangle_size(histogram):
    stack = []
    top = lambda: stack[-1]
    max_size = (0, 0, 0) # height, width and start position of the largest rectangle
    pos = 0 # current position in the histogram
    for pos, height in enumerate(histogram):
        start = pos # position where rectangle starts
        while True:
            if not stack or height > top().height:
                stack.append(Info(start, height)) # push
            elif stack and height < top().height:
                max_size = max(max_size, (top().height, (pos - top().start), top().start), key=area)
                start, _ = stack.pop()
                continue
            break # height == top().height goes here

    pos += 1
    for start, height in stack:
        max_size = max(max_size, (height, (pos - start), start), key=area)

    return max_size

def area(size):
    return size[0] * size[1]

#Triggers: Fonctions qui peuvent servir de trigger pour le refresh de l'affichage

def timer(depart,time_refresh):
    """Trigger fondé sur le temps,
    Detecte simplement si trop de temps s'est écoulé depuis le 
    dernier refresh.
    """
    return time()-depart>time_refresh

def gesture(frame):
    """ Fonction trigger fondé sur les gestes permettant 
    de determiner si la main présente dans le champ de 
    vision est ouvert ou fermée
    Sortie:
    - Fermée = 0
    - Ouverte = 1
    """

    #define the kernel, roi, rectangles
    kernel = np.ones((3,3),np.uint8)
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)

    # define range of skin color in HSV
    lower_skin = np.array([0,20,70], dtype=np.uint8)
    upper_skin = np.array([20,255,255], dtype=np.uint8)

    #extract skin colur imagw  
    mask = cv2.inRange(hsv, lower_skin, upper_skin)

    mask = cv2.dilate(mask,kernel,iterations = 1)

    #find contours
    contours,hierarchy= cv2.findContours(mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    #find contour of max area(hand)
    if len(contours)>0:
        cnt = max(contours, key = lambda x: cv2.contourArea(x))
            
        #approx the contour a little
        epsilon = 0.0005*cv2.arcLength(cnt,True)
        approx= cv2.approxPolyDP(cnt,epsilon,True)
           
        #make convex hull around hand
        hull = cv2.convexHull(cnt)
            
        #define area of hull and area of hand
        areahull = cv2.contourArea(hull)
        areacnt = cv2.contourArea(cnt)
          
        #find the percentage of area not covered by hand in convex hull
        arearatio=((areahull-areacnt)/areacnt)*100

        if arearatio<20:
            cv2.putText(frame,'close',(0,50), font, 2, (0,0,255), 3, cv2.LINE_AA)
            val = 1
        else:
            cv2.putText(frame,'Open',(0,50), font, 2, (0,0,255), 3, cv2.LINE_AA)
            val = 0


        cv2.imshow('gesture',frame)
        cv2.imshow('mask',mask)

        MODE = 1
        if MODE:
            return val
        else:
            cv2.drawContours(frame, hull, -1, (255,255,0), 3)
            return frame, val
    else:
        return(0)

def word():
    """Fonction trigger fondé sur le raconaissance vocale
    Si le keyword est prononcé alors on refresh l'affichage
    """
    pass
