import cv2
import numpy as np
#from function import max_rect
import time
import sys
import math
from matplotlib import pyplot

error = True
maxiter=10
iteration=0 # on commence pas à zero pour pas trouver la webcam de l'ordi.
while (iteration<maxiter and error):
    try:
        cam = cv2.VideoCapture(iteration)
        ret_val,img = cam.read()
        img = cv2.resize(img,(100,100))
        error=False
        print("Cam {} fonctionne!".format(iteration))
    except:
        iteration += 1
        print("Cam {} erreur...".format(iteration))
        cam.release()

if(iteration >= maxiter):
    sys.exit("Pas de camera trouvé. Sortie du programme")


rapport = []
precedent=0
temps = []
calcul = []

while True:
    try:
        depart = time.time()
        ret_val, img = cam.read()
        # flip the frame
        frame = cv2.flip(img,1)

        #define the kernel, roi, rectangles
        kernel = np.ones((3,3),np.uint8)
        pt1 = (100,0)
        pt2 = (500,200)
        roi = frame[pt1[1]:pt2[1],pt1[0]:pt2[0]]
        cv2.rectangle(frame,pt2,pt1,(0,255,0),0)

        hsv = cv2.cvtColor(roi,cv2.COLOR_BGR2HSV)

        # define range of skin color in HSV
        lower_skin = np.array([0,20,70], dtype=np.uint8)
        upper_skin = np.array([20,255,255], dtype=np.uint8)

        #extract skin colur imagw  
        mask = cv2.inRange(hsv, lower_skin, upper_skin)

        mask = cv2.dilate(mask,kernel,iterations = 1)

        #find contours
        contours,hierarchy= cv2.findContours(mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        
       #find contour of max area(hand)
        cnt = max(contours, key = lambda x: cv2.contourArea(x))
            
        #approx the contour a little
        epsilon = 0.0005*cv2.arcLength(cnt,True)
        approx= cv2.approxPolyDP(cnt,epsilon,True)
           
        #make convex hull around hand
        hull = cv2.convexHull(cnt)
            
         #define area of hull and area of hand
        areahull = cv2.contourArea(hull)
        areacnt = cv2.contourArea(cnt)
          
        #find the percentage of area not covered by hand in convex hull
        arearatio=((areahull-areacnt)/areacnt)*100

        temps.append(time.time()*100)
        # rapport.append(arearatio)
        rapport.append(float((arearatio+precedent)/2))
        precedent = arearatio

        font = cv2.FONT_HERSHEY_SIMPLEX
        try:
            if arearatio<15:
                cv2.putText(frame,'close',(0,50), font, 2, (0,0,255), 3, cv2.LINE_AA)
            else:
                cv2.putText(frame,'Open',(0,50), font, 2, (0,0,255), 3, cv2.LINE_AA)
        except:
            cv2.putText(frame,'Erreur',(0,50), font, 2, (0,0,255), 3, cv2.LINE_AA)

        calcul.append((time.time()-depart)*1000)
        #show the windows
        cv2.imshow('mask',mask)
        cv2.imshow('frame',frame)

        if cv2.waitKey(1) == 27:
            break  # esc to quit
    except:
        pass

cam.release()
cv2.destroyAllWindows()

pyplot.figure("Mesure du ratio")
pyplot.title('Ratio main/envellope convexe')
pyplot.plot(temps,rapport,label="rapport")
pyplot.plot(temps,calcul,label="temps de calcul (ms)")
pyplot.xlabel('Temps')
pyplot.ylabel('Rapport & tempde calcul (ms)')
pyplot.legend(loc='best')
pyplot.show()