import cv2
import numpy as np
from time import time
import screeninfo as scri

cs = scri.get_monitors()[0]# Normalment 0 c'est le principal
ds = scri.get_monitors()[1]# Normalement 1 c'est le secondaire

#Création de la fenêtre
window = "Projection"
cv2.namedWindow(window, cv2.WND_PROP_FULLSCREEN)
cv2.moveWindow(window, ds.x - 1, ds.y - 1)
cv2.setWindowProperty(window, cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)

full = np.zeros((ds.height,ds.width,3))
full = full.astype('uint8')
full[:] = (0, 0, 255) # Couleur
cv2.imshow(window,full)

depart = time()
while(time()-depart<2):
    # cv2.imshow(window,full)
    if(cv2.waitKey(1) == 27):
        # Destruction des instance crées
        cv2.destroyAllWindows() 

cv2.destroyAllWindows()
print("FIN")