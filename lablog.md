# LabLog

## 5 Juin

### Le traitement d'image c'est marrant

Deuxieme jour de stage. J'ai un peu continué à envisager des cas d'utilisation mais je pense être arrivé au bout de mon idée. J'attend demain pour confirmer ces idées avec Yvonne et Sinan.

J'ai commencé à regarder ce sur quoi je peux m'avancer. Je ne peux pas trop avoir accès au matos mécannique. J'aimerai savoir ce qu'il y a et dans quel état mais pour l'instant c'est impossible. 

J'ai donc regardé la partie soft avec les traitements d'image. J'ai commencé par la Homographie. J'ai réalisé un petit programme PoC qui distord une image (affiche) pour la mettre à l'aspect d'un mur en perspective. Le traitement est relativement simple donc je pense pouvoir l'inverser pour corrger la perspective de la projection quand le moment sera venu.
L'aprés midi j'ai regardé cette fois la détection des zones d'affichage. j'ai étudié le principe de fonctionnement du script de David et je l'ai repris en parti. Je vais essayer de le faire en python et de la modifié pour avoir une détection de zones d'affichage qui respecte au mieux la dimension de l'image à afficher. Pour l'instant mon scripte détecte une zone mais je n'ai pas encore visualisé si elle était cohérente.

## 6 Juin

### StackOverflow to the rescue

Je commence par chercher à améliorer ma détection de zone d'affichage. les résultats donnés par ma méthode ne sont pas convainquant. Je cherche si des articles / post parlent de ce renge de problèmes. Je tombe sur [celui la](https://stackoverflow.com/questions/51574829/how-to-algorithmically-find-the-biggest-rectangle-that-can-fit-in-a-space-with-o) mais il décrit une méthode en N^5 avec N le nombre de réclangle. Ayant beaucoup d'obstacle, ce n'est pas optimal pour moi.

Arretez tout c'est bon on à trouvé. Aprés avoir passé la journée à se tirer les cheveux sur des histoires d'accumulateurs, j'ai copié collé [une réponse sur StackOverflow](https://stackoverflow.com/a/52624078) et ça marche directement. J'ai plus qu'a comprendre le code...
Le pire c'est que ça marche avec toutes les images (sauf la table qui à beaucoup trop de détail) et rapidement. tellement rapidement que je vais essayer d'appliquer ça à une vidéo pour voir comment l'algo s'en sort.

C'est pas parfait mais ça marche... à améliorer.

Pour la récupération de l'écran, [ceci](https://www.youtube.com/watch?v=GWdrL8dt1xQ) est prométeur.

## 7 Juin

### Master of the monitors

Je commence par étudier la quetion de l'affichage de mes images générées. Je vex qu'elles soient projetées ce qui revient à l'afficher sur un moniteur secondaire. Heureusement on peut assigner des positions aux images cv2 et il ne reste plus qu'a connaitre la position voulue. Avec screeninfo j'ai bricolé un petit script qui permet de sélectionner l'écran à capturer et d'afficher sa retransmission sur l'autre. Normalement ça marche dans toutes les configurations. À tester avec un projecteur. Prochaine étape: adapter l'image capturée pour qu'elle soient affichée dans la zone détectée.

> Note: On commence à voir apparaitre des problèmes de fluidité lors de la capture de l'écran. Il va falloire ne pas trop surcharger cette boucle de capture/affichage pour ne pas agraver le problème. Peut être d'autres outils que pyscreenshot existent pour rendre ça plus smooth.

Bon il s'avère que récupérer une image, la redimensionner et la coller sur une atre c'est plus complexe que prévu. Je doute pas d'y arriver mais va falloire faire des choses propres.

### Réunion avec Sinan et Yvonne:
L'application est plus clair. Sinan met en garde sur les traitement informatiques de rectification de laperspective qui font beaucup perdre en qualité. On va donc essayer de rester le plus possible face au mur avec suffisement de degres de libertés. Je vais chercher un vidéoprojecteur qui remplit le plus de cases possible:

* Haute résolution (4k)
* Laser
* Sans fil
* Dimensions qui respectent la mécannique.
* fait le café

Pour l'instant on semble rourner autour des 3000-4000€ pour cocher le plus de chose donc ça va être un peu chaud.

On va également prévenir le service technique qu'on aimerait accrocher quelque choses au plafon. On va donc commencer à taffer sur ça.

Prochains points qui me dérange car je ne sais pas comment les aborder:
* Lien entre Arduino et Raspi. J'ai trouvé un [tuto](https://www.instructables.com/id/Connect-Your-Raspberry-Pi-and-Arduino-Uno/) et un autre [tuto](https://www.sunfounder.com/blog/rpi-ard/) qui semblent plutot complets mais je n'ai jamais testé ça donc j'attend de voir si ça marche bien.
* Commande des moteurs. Deux moteurs sont présents sur le montage mécannique. Le second est un servo donc normalement c'est simple à commander. Le premier n'est pas un servo et à l'air plus massif. Je sais pas comment le commander.

C'est parti pour un Weekend de 3 jours !

## 11 Juin
On repart avec le choix des projecteurs. J'ai constitué une liste plutot représentative de ce que l'on peut trouver sur le marché. J'attend une réponse et les remarques de la part deSinan et Yvonne pour savoir vers lequel on s'oriente.

J'ai repris le code de l'affichage et j'ai trouvé mes problèmes. Premièrement il fallait inverser les dimensiosn du resize pour que le collage puisse se faire aprés (facile mais fallait le trouver). Ensuite j'ai résolu le problème d'affichage en changeant les types. Le screenshot était en uint8 et le fond de base en float64. J'ai tout mis en uint8 et j'ai converti l'image capturé de BGR à RGB et les couleurs sont revenues à la normale. Je suis très content de ce code. 
Il reste toujours un problème de latence. Avec tout les traitements, il y à un décallage de ~2 secondes entre l'écran et se reproduction. Ce problème peut être réglé par un ordi plus puissant (peu probable puisque ça part sur raspi aprés) ou par le passage en C (facile pour openCV mais chiant pour toute la partie numpy). A voir si c'est vraiment un problème. en effet si on affiche des pages pdf, on s'en fout de la latence d'une image fixe. Je vais quand même essayer d'optimiser ma boucle.

J'ai ajouté la détection sur l'image de la webcam. Comme prévu c'est ultra leeeeeeeeennnnnt mais ça marche. Cependant le bruit sur les images consécutive fait que la zone choisie bouge un peu en permanence. Évidemment à terme le choix de la zone ne sera fait que périodiquement ou même seulement sur demande de l'utilisateur mais pas en permanence. Ou bien il faut faire quelque chose d'intéligent avec un changement de la zone seulement dans le cas d'une modification importante de l'espace disponible.

## 13 Juin

### Oui j'ai raté un jour...

Bon ba je suis reparti à glandouiller avec mon code toute la journée. J'ai commencé par faire une liste de "petits" projecteurs interessants pour mon projets. Le BenQ W1700 semble être un bon choix. Mais je n'ai toujorus pas de projo pour tester ce que je voulais donc je continue à m'amuser avec mon code.

Par contre ce que j'ai maintenant c'est une camera. Je m'évertue donc de la faire fonctionner en prévision de l'utiliser réellement. Il s'avère qu'il faut jouer à cache-cacher avec la camera qui change d'Id en permanence et pour des raison obscures. Mais quand on arrive à la trouver on peut faire des trucs cool. La détection de zone d'affichage en directe est pas optimale car ce traintement prend un peu de temps donc l'image devient un diaporama. Mais en efectuant cette recherche que de temps en temps, on peut se service la zone détectée pour afficher le bureau ou même ce qui filme la camera en direct comme si c'était projeté et c'est très cool.

## 14 Juin

### Trouver des stages c'est un peu de la réalité augmentée quand on y pense...

Bon j'ai commencé la journée en cherchant des stages pour clem et des demandes de visa. mais j'ai quand même taffé un peu.

Je continue à rajouter des features kikoolol:
* J'ai ajouté une appréciation de la zone d'affichage basée sur l'aire disponible. Un cadre rouge ou bleu entourera l'image selon si la zone est trop petite ou non. Ça marche bien mais mon critère est assez dur donc c'est pas simple de trouver une zone suffisante (mais ça se règle facillement).
* J'en ai marre de jouer à Cache Cache avec la webcam donc j'ai ajouté un parcour iteratif des index de camera entre 1 et 10 jusqu'a la trouver. C'est un peu sal parceque quand ça marche pas ça renvoit pas une erreur directe (juste un stdout). il faut appliquer une modification pour avoir une erreur que l'on peut attraper.Maintenant ça marche tout le temps.
* J'ai eu quelques cas d'application de l'image dans le cadre qui plantais donc j'ai rajouté une exception. dés que l'image est rechargée en générale tout revient en ordre. 

La partie mécannique à bien commencé. J'ai récupéré une arduino mega, une raspi sans carte SD, et un shield de controle de moteur (je pense pas en avoir besoin). J'ai réalisé un plan de cablage pour les moteurs mais j'ai pas encore tout ce qu'il faut. Le gros est un brushless qui nécéssite un ESC que je vais essayer de trouver. Le petit est un servo tout con mais qui va nécéssité une alimentation externe (la charge est plutot lourde). Les prochains objectifs sont donc:

* Commander le gros moteur.
* Commander le servo.
* Effectuer la liaison avec la raspi.
* Piloter les rotations depuis la raspi.

## 17 Juin

### Les mains dans l'arduino

En arrivant je suis directement descendu avec ma nacelle. objéctif du jour, faire tourner les moteurs. J'ai commencé par le petit servo. J'ai trouvé une alim 9V 1.5A et j'ai tout branché sur l'arduino. J'ai au final réussi à fair tourner le moteur mais pas efficacement. La liaison pivot entre la nacelle et le support du projo et pas terrible et ça frotte énormément. Si on supprime le support, les déplacements sont nikels (servo à vide). mais dés que l'on rajoute quoi que ce soit, plus rien ne marche ou alors à grand peine.

Pour le gros moteur maxon, il faut trouver les ESC pour le controller. A priori ils sont dans l'armoire mais il faut attendre confirmation pour ne pas tout cramer. Donc j'attend.

## 18 Juin

### Projecteur ?

Aujourd'hui je peux pas plus avancer sur la partie mécannique qu'hier. je vais donc essayer de projeter quelque chose et de faire fonctionner le combo camera/projecteur. Je vais emprunter le mini projecteur qui à été retrouvé et tester ça des que je peux. Je vais aussi essayer de completer mon schéma éléctrique pour avoir un plan claire de ce qu'il y à a faire sur la partie mécatronique.

J'ai eu le petit projecteur !
aprés avoir bien galéré à le branché il marche finalement et faut avouer qu'il est super impressionant ! Pour la taille qu'il fait il affiche une image vraiment belle même à plusieurs metres et avec les lumieres allumés. J'ai lancé mon programme dessus (qui à tout de suite tout détecté je suis très fier!). Je n'avait pas pensé au fait que l'image projetée influençais la prise de décision concernant la zone. Je vais donc corriger ça.
Le progrés n'est pas flagrant.pour une raison inconnue, l'image considérée pour séléectionnée la zone contient toujours l'image projetée même lorsque l'on ne projete rien quand on capture l'image. Je n'ai pas résolu à résoudre ce problème aujourd'hui. Je doute que ce soit un problème dans le code mais plus une mauvaise comprehension du fonctionnement de cv2. A voir demain.

## 19 juin

### Smart projector victory !... et surchauffe.

Je n'ai pas encor récupéré mon vidéo projecteur donc je vais commencer par relire et proprifier mon code pour être sur d'éliminer toutes les erreurs à la con.

Victoire ! Aprés avoir tenté un truc un peu moche ou je flood la camera de demande d'image, j'ai réussi à récupérer une image du set sans l'image projetée et mon code s'est mit à fonctionner comme prévu. Si la camera est bien orientée et fixe dans le repere du projo, alors il va s'adapter pour trouver la zone d'affichage la plus grande et y mettre l'image voulue. Ça marche vraiment pas trop mal mais évidement il est compliqué de garder la camera orientée correctement. 
Mon idée pour régler ce problème est de projecter un cadre rouge sur les bordes de la zone affichage puis de le détecter en utilisant uniquement le canal rouge de la camera et ainsi détecter la zone d'affichage possible (et ne chercher des zone plates que dans cette zone). Le problème est qu'avec mon projecteur actuel très faible en lumen, il est compliqué des détecter le cadre sur le canal rouge.

J'ai quand même persévéré. Je pense qu'il est possible de détecter le rectangle rouge avec une meilleur luminausité. j'ai donc contruit une boite pour entourer mon set et donc avoir un meilleur rendu. Je tente une approche avec la détection de contour et l'approximation du rectangle. En réglant bien le filtre de cany on arrive à avoir le rectangle du milieu. Malheureusement mon vidéo projecteur à surchauffé avant que je finise donc on verra ça demain.

## 20 Juin

### État des lieux

Journée Off, plein de trucs à faire

## 21 Juin

### Raspberry et Maxon.

Deux objectifs principaux aujourd'hui. Setup de la raspberry et setup du moteur maxon. Pour la raspberry j'ai reçu ma carte SD donc le setup devrait être assez simple. Pour la maxon j'ai reçu mon alim et un controleur avec sa doc. Il faut donc que je parcour la doc, que je reprogramme le controlleur, et que je cable tout pour tester.

Raspberry Pi:

- [X] Setup carte SD
- [] Setup network & SSH
- [] Setup X forwarding
- [] Test PWM
- [] Test Servo

Maxon:

- [X] Comprendre la doc
- [X] Reprogrammer le controlleur
- [] Cablage
- [] Test

## 24 Juin

### Qui à dit que les vapeurs de soudure rendhait comùplettmant conh !?

Objectif de la journée: faire tourner le moteur Maxon.
Résultat de la journée: meeh...

Aprés avoir passer la journer à faire des soudures de pendulants entre pas mal et un peu dégeus, le moteur est presque pret à tourner. Il ne passe pas encore tout les tests (notemment en raison d'un problème sur le capteur à effet hall) mais il est "pillotable" via le logiciel ESCON Studio. Je suis donc content parceque je sais que le moteur et le controleurs fonctionnent mais il faut encor bien proprifier les contacts pour élliminer toutes les sources d'erreur. 

## 1 Juillet

### Reprenons les bonnes habitudes

Bon déjà je suis arrivé au labo à 14h30 pour bien commencer la journée. En plus j'ai oublié la raspberry pi sur la quelle je taff. Donc je vais essayer de pofiner mon algo de détection de la zone de projection ce sera un bon point. 

## 4 Juillet

### J'ai jamais été bon en papier cadeau

J'ai le choix: Abandonner le Jetson et acheter une raspi 4K (très tentant) ou persévérer avec le Jetson et écrire mon wrapper pour pilloter les GPIO dans Python via du C. En attendant une réponse du Forum Nvidia et le devis de Kubii je vais essayer le wrapper.

Première étape, faire parler le C et le Python. Pour ça j'utilise ce très bon [tuto](https://pgi-jcns.fz-juelich.de/portal/pages/using-c-from-python.html). Et aprés quelques minutes de code, Victoire! J'arrive donc à appeler le C dans du Python mais encore faut il que mon C fasse quelque chose d'utile...

L'appel aux fonctions C de gestion du GPIO onctionne mais je sens bien que ce n'est pas l'utilisation prévue. Je me heurte à des problèmes des permission et de devices occupées. Je ne pene pas que ce soit la bonne solution. Je préfèrerai attendre la raspi4 et travailler avec ça.

Pour ne pas perdre ma journée j'ai encor un peu taffé sur la détection des la zone d'affichage. Maintenant je fait marcher mon script sur des images de cmera et pas des images fixes. Le problème est que j'ai utilisé un carton rouge pour simuler l'affichage et que la pose d'un object physique modifie l'illumination des objets alentours et donc crée des contours parasites. Heureusement ces contours sont petits et on peut les éliminer en prenant juste le plus grand. Maintenant ça marche super bien !

## 5 Juillet

### Pimp my sublime text

Bon la journée s'annonce légère vu que je peux rien faire tant que j'ai pas mes raspi. Je vais faire un peu d'administratif (raspi et arpe) et aussi préparer mon code pour être exporté sur la raspi des que je l'ai. Je vais essayer de commencer à voir mon projet dans son ensemble et faire un code un peu plus 'prod worthy'. Mais d'abord j'engueule la DRH de l'ENS...

## 10 Juillet

### Si seulement re2o faisait parti de mon stage...

Bon je suis un peu au chomage technique tant que je n'ai pas mon matos (raspi et video projecteur). J'essaye de trouver laurent pour avancer sur la partie mécannique mais je le trouve pas.
J'ai proprifié mon code et la recherche et le découpage sur la zone d'affichage possible se passe bien. Étonement ça se passe mieux quand la seconde image contient la zone plutot que l'inverse mais je sais pas du tout pourquoi. Ça ne change rien à l'exploitation.

## 11 Juillet

### Mecanique 

Je suis arrivé à midi mais on m'a dit que Laurent était la donc je vais essayer de le trouver cet aprem pour voir comment faire avancer la partie mécannique. Il y à deux points sur lesquelles travailler: Le rail au plafon. Comment le mettre en place, ses dimensions, son emplacement. Et la liaison pivot de la nacelle. Si cette liaison n'est pas améliorer il n'y à pas du tout moyen de faire pivoter le vidéoprojecteur en position dessus. Bref ya un peu de taf.

Toujour aucune trace de Laurent à 14h30...

## 18 Juillet

### Perspective

La orrection de perspective avec cv2 est particulierement chiante. Principalement parceaue l'on ne peut pas savoir a l'avance la taille de l'image finale, la position de l'inage initiale dans l'image finale. Dans un context ou le placement de l'image doit etre precis, cela rend le travail perticulierement chiant. Je pense aue je ne vais ps m'attaquer a ce probleme dans ce stage.




