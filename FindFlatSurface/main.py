"""
###############################################################
Code faisant parti du stage de M1 à l'ISRI - Sorbonne Université

Auteur: Arthur Grisel-Davy

But: Ce code est le programme principale. Il à pour but d'initialisé les 
différents périphériques et de rentrer dans la boucle principale.
Dans ce code, la zone d'affichage est choisie, la ressource à aficher
est redimenssionée pour cette zone, et le tout est affiché.
###############################################################
"""
import os
import cv2
import sys
import numpy as np
import screeninfo as scri
import pyscreenshot as pss
import matplotlib.pyplot as plt
from time import time,sleep
from utils import (
    max_rect,   
    find_webcam,
    find_zone_affichage,
    find_displayable_zone,
    timer,
    gesture,
    word,
)

cam = find_webcam()

#MODES:
# Modes d'affichage possibles selon les configurations
# 0 = 2 ecrans, prise sur le premier, affichage sur le second
# 1 = 1 ecran, affichage d'une image fixe
# 2 = 2 ecrans, image fixe
MODE = 2

# TRIGGERS:
# Modes de déclenchements de la recherche de position d'affichage
# time = définie une fréquence de rafraichissement de l'affichage
# gesture = capte un geste (main ouverte) comme signal
# word = detecte un mot 'refresh' comme signal


#Gestion des écrans pour l'affichage
if MODE == 1:
    # MODE 1 ecran, image fixe
    #cs = scri.get_monitors()[0]# Normalment 0 c'est le principal
    ds = scri.get_monitors()[0]# Normalement 1 c'est le secondaire
elif MODE == 0 or MODE ==2:
    # MODE 2 ecrans, capture
    cs = scri.get_monitors()[0]# Normalment 0 c'est le principal
    ds = scri.get_monitors()[1]# Normalement 1 c'est le secondaire


#Création de la fenêtre
window = "Projection"
cv2.namedWindow(window, cv2.WND_PROP_FULLSCREEN)
cv2.moveWindow(window, ds.x - 1, ds.y - 1)
cv2.setWindowProperty(window, cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)

back = np.zeros((ds.height,ds.width,3))# Création du fond à la taille de l'affichage
back = back.astype('uint8')
cv2.imshow(window, back)

pt1 = pt2 = (0,0)
# Recuperation de la zone d'affichage possible totale (atteignable par le projecteur)
x,y,w,h = find_displayable_zone(cam,window,ds)

stop = False
time_refresh=6
timer_find = time()-time_refresh
FLAG_REFRESHED = False


if MODE==1 or MODE ==2:
    img_disp = cv2.imread('images/isir.png',1)
    ratio = float(img_disp.shape[1]/img_disp.shape[0])

elif MODE == 0:
    ratio = float(cs.width/cs.height)

blank = np.zeros((ds.height,ds.width,3))
blank = blank.astype('uint8')

print('ratio: {}'.format(ratio))
print('Boucle principale')

while(not stop):

    # TRIGGER time:
    #if timer(timer_find,time_refresh):
    if time()-timer_find>time_refresh:

    # TRIGGER gesture:
    # ret_val,img = cam.read()
    # if gesture(img):
    #     sleep(1)

    # TRIGGER word
    # if ... :

        # On cherche à nouveau la meilleure position
        print('Refresh')
        cv2.imshow(window,blank)  # Affichage du fond noir (pas d'affichage)

        start_flood = time()
        while(time()-start_flood<0.4):  # purger les anciennes images avec la projection
            ret_val,img = cam.read()
            if(cv2.waitKey(1) == 27):
                pass

        mur_cropped = img[y:y+h,x:x+w] # recuperation uniquement de la zone d'affichage possible
        pt1,pt2,area = find_zone_affichage(mur_cropped,ratio,ds)  # detection de la meilleur zone d'affichage

        back = np.zeros((ds.height,ds.width,3))# Création du fond à la taille de l'affichage
        back = back.astype('uint8')

        timer_find=time()

        cv2.imshow(window,blank)

        FLAG_REFRESHED = True

    # elif time()-timer_find>0.90*time_refresh:
    #     cv2.imshow(window,blank)
    #     ret_val,img = cam.read()

    else:
        # Pour afficher l'écran principale
        if MODE == 0:
            #Capture
            img = cv2.cvtColor(np.array(pss.grab()),cv2.COLOR_BGR2RGB)

            #Calcule
            img = img[cs.y:cs.y+cs.height,cs.x:cs.x+cs.width]
            img = cv2.resize(img,(pt2[1]-pt1[1],pt2[0]-pt1[0]))

            #Affichage
            try:
                back[pt1[0]:pt2[0],pt1[1]:pt2[1]] = img[:,:]
            except:
                print("Application de l'image ratée. Continue...")

        # Pour afficher une image chargee avant la boucle dans img_disp
        # On ne rentre dans le calcule de l'image QUE si les paramètres ont changés (refreshed)
        # sinon, on laisse l'affichage comme il est car rien n'a changé.
        if ((MODE == 1 or MODE == 2) and FLAG_REFRESHED):
            # Calcule
            img = cv2.resize(img_disp,(pt2[1]-pt1[1],pt2[0]-pt1[0]))

            # Affichage
            try:
                back[pt1[0]:pt2[0],pt1[1]:pt2[1]] = img[:,:]
                FLAG_REFRESHED = False
            except Exception as e:
                print(e)
                print("Application de l'image ratée. Continue en mode {}...".format(MODE))

        cv2.imshow(window, back)

    if(cv2.waitKey(1) == 27):
        # Destruction des instance crées
        cam.release()
        del cam
        cv2.destroyAllWindows()  
        stop = True

print("FIN")
