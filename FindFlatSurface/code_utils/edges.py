import cv2
import pyscreenshot as pss
import numpy as np
import screeninfo as scri


cs = scri.get_monitors()[0]# Normalment 0 c'est le principal
ds = scri.get_monitors()[1]# Normalement 1 c'est le secondaire


stop = False

while not stop:
    img = cv2.cvtColor(np.array(pss.grab()),cv2.COLOR_BGR2RGB)
    img = img[cs.y:cs.y+cs.height,cs.x:cs.x+cs.width]
    edges = cv2.Canny(img,20,30)
    edges_petit = cv2.resize(edges,(int(cs.width/2),int(cs.height/2)))
    ret,edges_petit = cv2.threshold(edges_petit,127,255,cv2.THRESH_BINARY)


    kernel = np.ones((5,5),np.uint8)
    edges_petit_dilate = cv2.dilate(edges_petit,kernel,iterations = 1)

    cv2.imshow("edges petit",edges_petit_dilate)

    for i in range(300):
        edges_petit[i,i]=255

    cv2.imshow("soustraction",cv2.subtract(edges_petit,edges_petit_dilate))

    if(cv2.waitKey(1) == 27):
        # cv2.imwrite("edges.png",edges_petit)
        # cv2.imwrite("edges_dilate.png",edges_petit_dilate)
        cv2.imwrite
        cv2.destroyAllWindows()  
        stop = True

print("FIN")