"""
###############################################################
Code faisant parti du stage de M1 à l'ISRI - Sorbonne Université

Auteur: Arthur Grisel-Davy

But: Ce code est le programme principale. Il à pour but d'initialisé les 
différents périphériques et de rentrer dans la boucle principale.
Dans ce code, la zone d'affichage est choisie, la ressource à aficher
est redimenssionée pour cette zone, et le tout est affiché.
###############################################################
"""


import numpy as np
import cv2
import pyscreenshot as pss
import screeninfo
from time import time,sleep
import sys, os

from function import max_rect

# Gestion des écrans pour l'affichage >>>>>>>>>>>>>>>>
cs = screeninfo.get_monitors()[0]# Normalment 0 c'est le principal
ds = screeninfo.get_monitors()[1]# Normalement 1 c'est le secondaire


error = True
maxiter=10
iteration=1 # on commence pas à zero pour pas trouver la webcame de l'ordi.
while (iteration<maxiter and error):
    try:
        cam = cv2.VideoCapture(iteration)
        ret_val,img = cam.read()
        img = cv2.resize(img,(100,100))
        error=False
        print("Cam {} fonctionne!".format(iteration))
    except:
        iteration += 1
        print("Cam {} erreur...".format(iteration))
        cam.release()

if(iteration >= maxiter):
    sys.exit("Pas de camera utilisable. Sortie du programme")


# print("Écran de projection: width={},heigh={}".format(ds.width,ds.height))

window = "Projection"
cv2.namedWindow(window, cv2.WND_PROP_FULLSCREEN)
cv2.moveWindow(window, ds.x - 1, ds.y - 1)
cv2.setWindowProperty(window, cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)

# <<<<<<<<<<<<<<<<<<<<<<<<

def find(mur):
    mur = mur[int(res_cam[0]*bornes[0]):int(res_cam[0]*bornes[1]),int(res_cam[1]*bornes[2]):int(res_cam[1]*bornes[3])]
    edges = cv2.Canny(mur,20,200) # detect objects
    h,w,lc,br=max_rect(edges) # Find max rectangle

    x,y = edges.shape # get geometry
    # print("Image mur: x={},y={}".format(x,y))
    cv2.rectangle(edges,(lc,br-h),(lc+w,br),(100,255,0),2)
    cv2.imshow("Zone choisie",edges)
    # print("Plus grand rectangle trouvé en ({}:{}) de taille (width={}:height={}) en {} secondes)".format(lc,br-h,w,h,round(fin,3)))

    # Le rectangle rendu est le plus grand rectangle possible sans obstable.
    # Mais ce rectangle ne respecte peut être pas les dimensions de l'image à afficher.
    # On va donc tronquer ce rectangle pour avoir un ratio correcte.
    ratio = cs.width/cs.height

    if(1.8*h>w):
        # cas rectangle trop haut
        new_h = w/1.8
        br = br-(h-new_h)/2 #on remonte la bottom row de seulement la moitié pour garder le centre
        h=new_h
    else:
        # cas rectangle trop large
        new_w = h*1.8
        lc = lc+(w-new_w)/2
        w = new_w

    # Recalcule des points
    pt1 = (int((br-h)/x*ds.height), int(lc/y*ds.width))
    pt2 = (int(br/x*ds.height), int((lc+w)/y*ds.width))

    return(pt1,pt2,w*h,mur)

ret_val,mur = cam.read()
res_cam = mur.shape
# print("Image: {},{}".format(res_cam[0],res_cam[1]))
bornes = [1/5,4/5,1/5,4/5] # bornes de vision de la camera [left,right,top,bottom] pour fitter la projection
# print("Découpage: {}-{},{}-{}".format(res_cam[0]*bornes[0],res_cam[0]*bornes[1],res_cam[1]*bornes[2],res_cam[1]*bornes[3]))
pt1,pt2,area,mur = find(mur)

# Adaptation des affichages >>>>>>>>>>>>>>>>>>
# print("pt1: ({}), pt2: ({})".format(pt1,pt2))

back = np.empty((ds.height,ds.width,3))# Création du fond à la taille de l'affichage
# back = back.astype('uint8') # Pour s'adapter au type du screenshot

# print("Création du fond de taille {}".format(back.shape))
# print("L'image sera resize à (height={}:width={})".format(pt2[0]-pt1[0],pt2[1]-pt1[1]))
# print("Zone du fond à changer: de {} à {}".format(pt1,pt2))
# <<<<<<<<<<<<<<<<<<<<<<<<<

#Affichage de l'image voulue >>>>>>>>>>>>>>>>>>>
stop = False
timer_find = time()
depart = time()
bornes = [1/5,4/5,1/5,4/5] # bornes de vision de la camera [left,right,top,bottom]
# Savoir si on vient de changer le cadre pour éviter d'afficher
# le rectangle de couleur à chaque fois
refreshed = True

while(not stop):

    if(time()-timer_find>5):
        # On cherche à nouveau la meilleure position
        blank = np.zeros((ds.height,ds.width,3))# Création du fond à la taille de l'affichage
        # cv2.rectangle(back,(pt1[1]-2,pt1[0]-2),(pt2[1]+2,pt2[0]+2),(255,0,0),2)
        blank = blank.astype('uint8')

        cv2.imshow(window,blank)
        start_flood = time()
        while(time()-start_flood<0.1):
            ret_val,mur = cam.read()
        pt1,pt2,area,mur = find(mur)
        timer_find = time()

        back = np.empty((ds.height,ds.width,3))# Création du fond à la taille de l'affichage
        back = back.astype('uint8') # Pour s'adapter au type du screenshot

        # refreshed = True

    elif(time()-timer_find>4.75):
        blank = np.zeros((ds.height,ds.width,3))# Création du fond à la taille de l'affichage
        blank = blank.astype('uint8')
        cv2.imshow(window,blank)

    else:
        refreshed = False
        # pt1,pt2,mur = find("camera",cam)
        # back = cv2.resize(mur,(ds.width,ds.height))

        # Pour afficher l'écran principale
        # img = cv2.cvtColor(np.array(pss.grab()),cv2.COLOR_BGR2RGB)
        # img = img[cs.y:cs.y+cs.height,cs.x:cs.x+cs.width]

        # Pour afficher la vue de la camera

        ret_val, img = cam.read()
        img = cv2.resize(img,(pt2[1]-pt1[1],pt2[0]-pt1[0]))

        # if (refreshed):
        #     if(area < ds.width/7*ds.height/7):
        #         cv2.rectangle(back,(pt1[1]-2,pt1[0]-2),(pt2[1]+2,pt2[0]+2),(0,0,255),2)

        #     else:
        #         cv2.rectangle(back,(pt1[1]-2,pt1[0]-2),(pt2[1]+2,pt2[0]+2),(255,0,0),2)

        try:
            back[pt1[0]:pt2[0],pt1[1]:pt2[1]] = img[:,:]
        except:
            print("Application de l'image ratée. Continue...")

        cv2.imshow(window, back)

    if(cv2.waitKey(1) == 27):
        cam.release()
        cv2.destroyAllWindows()  
        stop = True
# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

print("FIN")