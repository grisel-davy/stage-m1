import cv2
import numpy as np
#from function import max_rect
import time
import sys

error = True
maxiter=10
iteration=1 # on commence pas à zero pour pas trouver la webcam de l'ordi.
while (iteration<maxiter and error):
    try:
        cam = cv2.VideoCapture(iteration)
        ret_val,img = cam.read()
        img = cv2.resize(img,(100,100))
        error=False
        print("Cam {} fonctionne!".format(iteration))
    except:
        iteration += 1
        print("Cam {} erreur...".format(iteration))
        cam.release()

if(iteration >= maxiter):
    sys.exit("Pas de camera trouvé. Sortie du programme")


while True:
    ret_val, img = cam.read()
    # img = cv2.flip(img, 1)
    edges = cv2.Canny(img,60,100)
    taille = img.shape
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret, seg = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    
    cv2.imshow('Webcam', img)
    # cv2.imshow('Segmentation', seg)
    # cv2.imshow('Edges',edges)

    if cv2.waitKey(1) == 27:
        break  # esc to quit
print(np.array(img).dtype)
print(np.array(img).shape)


cam.release()
cv2.destroyAllWindows()
